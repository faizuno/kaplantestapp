import { Injectable } from "@angular/core";

@Injectable({
    providedIn : 'root'
})
export class UtilityService{
    
    private monthNames : Array<string> = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    private weekNames : Array<string> = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    getFormattedDate(value: Date){
        let day: String = this.weekNames[value.getDay()].slice(0, 3);
        let month: String = this.monthNames[value.getMonth()];
        let date: String = value.getDate().toString()
        let year: String = value.getFullYear().toString()

        return day + ", " + month + " " + (date.length > 1 ? date : "0" + date) + ", " + year
    }
}