import { UtilityService } from "./utility.service";

describe("utilityService", () =>{
    let utilityService: UtilityService;
    beforeEach(() => {
        utilityService = new UtilityService();
      });
    
    it("should have the date formatted with preceeding zero.", () =>{
        let date = new Date()
        date.setFullYear(2019);
        date.setDate(6);
        date.setMonth(10);
        let formattedValue = utilityService.getFormattedDate(date);
        expect(formattedValue).toEqual("Wed, November 06, 2019");
    });

    it("should have the date formatted.", () =>{
        let date = new Date()
        date.setFullYear(2019);
        date.setDate(10);
        date.setMonth(10);
        let formattedValue = utilityService.getFormattedDate(date);
        expect(formattedValue).toEqual("Sun, November 10, 2019");
    });
});