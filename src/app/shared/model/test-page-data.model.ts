export class TestPageModel {
    title: string;
    description:string;
    instructorName:string;
    instructorPhotoUrl:string;
    subjectPhotoUrl:string;
    time: Date;
}
