import { Component, OnInit } from '@angular/core';
import { routeConstant, applicationConstant } from 'src/environments/environment';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string = applicationConstant.title;
  routes = routeConstant;

  constructor(private titleService: Title){}

  ngOnInit(){
    this.titleService.setTitle(this.title)
  }

}
