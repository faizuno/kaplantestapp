import { Component } from '@angular/core';
import { routeConstant } from 'src/environments/environment';



@Component({
    templateUrl: "./home-page.component.html",
    styleUrls: ["./home-page.component.css"]
})

export class HomePageComponent{
   routes = routeConstant
}