import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { routeConstant } from 'src/environments/environment';
import { HomePageComponent } from './components/home-page/home-page.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: routeConstant.rootPath + routeConstant.homePagePath,
    pathMatch: 'full'
  },
  {
    path: routeConstant.homePagePath,
    component: HomePageComponent
  },
  { 
    path: routeConstant.testModulePath, 
    loadChildren: () => import('src/app/modules/test/test.module').then(m => m.TestModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
