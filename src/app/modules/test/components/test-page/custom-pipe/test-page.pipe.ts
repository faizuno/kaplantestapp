import { Pipe, PipeTransform } from '@angular/core';
import {UtilityService} from 'src/app/shared/utility.service';

@Pipe({name: 'groupTestDataByDate'})
export class GroupTestData implements PipeTransform {
    constructor(private utility: UtilityService){

    }
    transform(collection: Array<any>): Array<any> {
        let property: string = 'time';
        if(!collection) {
            return null;
        }

        const groupedCollection = collection.reduce((previous, current)=> {
            if (typeof current[property] !==  typeof Date){
                current[property] = new Date(current[property])
            }
            current[property].setHours(0,0,0,0);
            let value : string = this.utility.getFormattedDate(current[property])
            if(!previous[value]) {
                previous[value] = [current];
            } else {
                previous[value].push(current);
            }

            return previous;
        }, {});

        let returnCollection = Object.keys(groupedCollection).map(
            key => ({ 
                key,
                 value: groupedCollection[key]
                 }));

        returnCollection.sort((a, b) => (a.value[0].time > b.value[0].time) ? 1 : -1);
        
        return returnCollection;
    }
}