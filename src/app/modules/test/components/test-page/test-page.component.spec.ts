import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TestPageComponent } from './test-page.component';
import { GroupTestData } from './custom-pipe/test-page.pipe';
import { HttpClientModule } from '@angular/common/http';
import { TestPageDataService } from 'src/app/services/test-page-data.service';
import { TestPageModel } from 'src/app/shared/model/test-page-data.model';
import { Observable, observable } from 'rxjs';
import { AppError } from 'src/app/shared/errors/app.errors';
import { ConstantPool } from '@angular/compiler';

describe('ProductsComponent', () => {
  let component: TestPageComponent;
  let service: TestPageDataService;
  const testFormData: TestPageModel[] = [
    {
      title:"Nulla convallis dolor quis erat.",
      description:"Sed hendrerit luctus finibus. Sed justo dui, vulputate ac suscipit condimentum, porttitor sed dolor. Ut eu justo at metus dapibus facilisis a quis libero. Integer lectus turpis, pretium a tincidunt.",
      instructorName:"Erat Libero",
      instructorPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=C&w=60&h=60",
      subjectPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=C&w=60&h=60",
      time:new Date("2016-01-03 22:00:00")
    },
    {
      title:"Pellentesque sagittis porttitor tincidunt. Sed.",
      description:"Curabitur eu velit vitae massa varius rhoncus. Proin eu ligula venenatis, consequat libero maximus, varius lorem. Morbi a dignissim nibh. Suspendisse eget ornare nunc, sollicitudin lacinia elit. Sed in volutpat.",
      instructorName:"Scelerisque Via",
      instructorPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=D&w=60&h=60",
      subjectPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=B&w=60&h=60",
      time:new Date("2016-01-01 21:00:00")
    },
    {
      title:"Phasellus a interdum purus, non.",
      description:"Pellentesque bibendum, nulla tincidunt consequat rutrum, sem lacus mattis quam, cursus semper lectus nibh id diam. Duis ullamcorper, odio ac blandit pretium, purus est varius ante, eu aliquam elit tortor.",
      instructorName:"Cras Ac",
      instructorPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=B&w=60&h=60",
      subjectPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=A&w=60&h=60",
      time:new Date("2016-01-04 21:00:00")
    },
    {
      title:"Donec viverra, magna ut porttitor",
      description:"Maecenas finibus ullamcorper aliquam. Integer eros neque, placerat id convallis non, rutrum tempor nisi. In venenatis vulputate feugiat. Vivamus porttitor, odio sit amet volutpat maximus, magna est maximus sapien, et.",
      instructorName:"Posuere Una",
      instructorPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=E&w=60&h=60",
      subjectPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=A&w=60&h=60",
      time:new Date("2016-01-03 20:00:00")
    },
    {
      title:"In quis elit ut ipsum.",
      description:"Praesent fermentum tortor non arcu imperdiet, egestas vestibulum augue tempus. Nunc sollicitudin tincidunt metus placerat luctus. Praesent at finibus nibh. Donec auctor feugiat hendrerit. Nulla massa augue, mattis quis fermentum.",
      instructorName:"Aliquam Nisl",
      instructorPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=D&w=60&h=60",
      subjectPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=C&w=60&h=60",
      time:new Date("2016-01-05 19:00:00")
    },
    {
      title:"Ut consequat risus id lacus.",
      description:"Nunc hendrerit blandit elit sed rhoncus. Sed interdum tempus enim vel ornare. Nulla facilisi. Morbi rhoncus turpis in justo sollicitudin, sit amet varius magna fringilla. Fusce porta magna neque, nec.",
      instructorName:"Vestibulum Ante",
      instructorPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=B&w=60&h=60",
      subjectPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=A&w=60&h=60",
      time:new Date("2016-01-05 22:00:00")
    },
    {
      title:"Sed mauris dui, ornare ut.",
      description:"Vivamus pulvinar, nisl fermentum cursus tincidunt, tortor justo dignissim metus, consectetur facilisis nulla tellus ut nisi. Cras in lorem neque. Vivamus sed odio in libero finibus consequat. Maecenas facilisis nisi.",
      instructorName:"Integer Laciana",
      instructorPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=E&w=60&h=60",
      subjectPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=B&w=60&h=60",
      time:new Date("2016-01-01 20:00:00")
    },
    {
      title:"In hac habitasse platea dictumst.",
      description:"Suspendisse consequat egestas posuere. Integer diam diam, gravida ac condimentum a, vulputate et quam. Fusce eleifend leo sed diam cursus, nec ultrices orci luctus. Vivamus eget eros aliquam, suscipit sapien.",
      instructorName:"Ipsum Primis",
      instructorPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=A&w=60&h=60",
      subjectPhotoUrl:"https://placeholdit.imgix.net/~text?txtsize=34&txt=D&w=60&h=60",
      time:new Date("2016-01-03 21:00:00")
    }
  ]; 

  beforeEach(() => {
    service = new TestPageDataService(null);
    component = new TestPageComponent(service);
  });

  it('should set products property with the items returned from the server', () => {
    spyOn(service, 'getTestData').and.callFake(() => {
      return  new Observable(observe =>{
        observe.next(testFormData);
        observe.complete();
     });
    });
    component.ngOnInit();
    
    expect(component.testDataCollection).toEqual(testFormData);
  });

  it('should have same data in the observable view', () => {
    spyOn(service, 'getTestData').and.callFake(() => {
      return  new Observable(observe =>{
        observe.next(testFormData);
        observe.complete();
     });
    });
    component.ngOnInit();
    component.testViewData.subscribe(data=>{
      expect(component.testDataCollection).toEqual(data);
    })
  });

  it('should set the error property if server returns an error when getting test data', () => {
    spyOn(service, 'getTestData').and.returnValue(new Observable(observe=>{
      throw('server error');
    }));
    expect(component.error).not.toBeDefined();
    component.ngOnInit();
    expect(component.error).toBeDefined();
    expect(component.error.originalError).toEqual('server error');
  });
});