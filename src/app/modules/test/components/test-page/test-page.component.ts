import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { TestPageDataService } from 'src/app/services/test-page-data.service';
import { Observable, observable } from 'rxjs';
import { TestPageModel } from 'src/app/shared/model/test-page-data.model';
import { AppError } from 'src/app/shared/errors/app.errors';



@Component({
    templateUrl: "./test-page.component.html",
    styleUrls: ["./test-page.component.css"]
})

export class TestPageComponent implements OnInit{
    testDataCollection : TestPageModel[] =[];
    testViewData : Observable<TestPageModel[]>;
    error : AppError;
    constructor(private service: TestPageDataService){

    }

    ngOnInit() { 
        this.service.getTestData().subscribe(data => {
            this.testDataCollection = data;

            this.testViewData = new Observable(observe =>{
               observe.next(this.testDataCollection);
               observe.complete();
            });
        }, error => {
                this.error = new AppError(error);
                console.log(error);
        });
    }
}