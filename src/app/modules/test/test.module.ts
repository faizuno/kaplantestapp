import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule} from '@angular/common';
import { TestPageComponent } from './components/test-page/test-page.component';
import { TestRoutingModule } from './test-routing.module';
import { GroupTestData } from './components/test-page/custom-pipe/test-page.pipe';

@NgModule({
    imports:[
        CommonModule,   
        TestRoutingModule
    ],
    declarations:[
        TestPageComponent,
        GroupTestData
    ],
    providers:[
    ],
})

export class TestModule{
}
