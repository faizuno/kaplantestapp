import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestPageComponent } from 'src/app/modules/test/components/test-page/test-page.component';
import { routeConstant } from 'src/environments/environment';


const routes: Routes = [
    { path: routeConstant.testPagePath, component: TestPageComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestRoutingModule { }
