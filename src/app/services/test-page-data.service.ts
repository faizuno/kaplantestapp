import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TestPageModel } from '../shared/model/test-page-data.model';
import { HttpClientTestApp } from '../core/http/httpclient.service';

@Injectable({
  providedIn: 'root'
})
export class TestPageDataService {

    constructor(private httpClient : HttpClientTestApp){}

    public getTestData(): Observable<TestPageModel[]> {
        return this.httpClient.Get<TestPageModel[]>("./assets/testdata.json");
    } 
}