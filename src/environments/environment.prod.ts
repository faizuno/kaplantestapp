export const environment = {
  production: true
};
export const applicationConstant = {
  title: 'KaplanTestAppProd'
}
export const routeConstant ={
  rootPath: "/",
  routeSeparator: "/",
  homePagePath: "HomeProd",
  testModulePath: "TestProd",
  testPagePath: "TestPageProd",
}