# Pre-Requisites

Install the below requisites on the machine
Angular CLI: 8.1.3
Node: 12.6.0
Angular: 8.1.3

# TestApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.

## Install Dependencies

Navigate to the folder and run `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
